/*


Detect Mozi botnet scanner requests


*/
include "../utils/http.yar"

rule mozi_a {
meta:
    author = "Jon Heise"
strings:
    $mozi = /[Mm]ozi/
condition:
    $mozi and HTTP_LINK
}
